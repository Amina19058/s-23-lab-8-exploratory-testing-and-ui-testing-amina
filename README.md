# Lab 8 - Exploratory testing and UI

I decided to test [Wikipedia](https://en.wikipedia.org) website, the English one.

Test cases

- Case 1: Test search

| Action                                    | Result                   |
|-------------------------------------------|--------------------------|
| Open website https://en.wikipedia.org     | Website is opened        |
| Press loupe icon to start searching       | Search field opened      |
| Search for "Software quality"             | Search field is filled   |
| Press "Search"" button                    | Search result appeard    |

- Case 2: Test language change

| Action                                    | Result                   |
|-------------------------------------------|--------------------------|
| Open website https://en.wikipedia.org     | Website is opened        |
| Press language selection button           | Lang options appeard     |
| Choose russian                            | Page in russian lang     |
