import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class WikiTestClass(unittest.TestCase):

    _wiki_url = "https://en.wikipedia.org"

    def setUp(self):
        self.driver = webdriver.Safari()
        self.driver.implicitly_wait(10)
        self.driver.get(self._wiki_url)
    
    def test_wiki_main_title(self):
        print("Page title is: " + self.driver.title)
        assert "Wikipedia, the free encyclopedia" in self.driver.title

    def test_search(self):

        request = "Software quality"

        loupe_icon = self.driver.find_element(By.XPATH, '//*[@id="p-search"]/a')
        assert loupe_icon.is_displayed()

        loupe_icon.click()
        search_field = self.driver.find_element(By.CLASS_NAME, 'cdx-text-input__input')
        assert search_field.is_displayed()

        search_field.send_keys(request)
        assert search_field.get_attribute('value') == request

        search_field.send_keys(Keys.RETURN)
        sleep(3)
        search_result_title = self.driver.find_element(By.CLASS_NAME, 'mw-page-title-main').text
        assert request in search_result_title

    def test_language_change(self):

        language_selection_dropdown = self.driver.find_element(By.XPATH, '//*[@id="p-lang-btn"]')
        assert language_selection_dropdown.is_displayed()

        language_selection_dropdown.click()

        ru_language_option = self.driver.find_element(By.XPATH,
                                                      '/html/body/div[1]/div[2]/div[1]/div/ul/li[1]')
        assert ru_language_option.is_displayed()

        ru_language_option.click()
        sleep(3)
        assert 'ru' == self.driver.find_element(By.XPATH, "//html").get_attribute('lang')


    def tearDown(self):
        self.driver.close()


if __name__=='__main__':
    unittest.main()
